'use strict'
// first exercise
let a = '$100'
let b = '300$'

let regEx = /\d+/

let sum = +a.match(regEx) + +b.match(regEx)

console.log(sum)
// second one
let message = ' привет, медвед      '

message = message.trim()
message = message.charAt(0).toUpperCase() + message.slice(1);

console.log(message)
// third
let age = +prompt('Сколько вам лет?', '')

if (age >= 0 && age <= 3) {
    alert(`Вам ${age} лет и вы младенец`)
} else if (age >= 4 && age <= 11) {
    alert(`Вам ${age} лет и вы ребенок`)
} else if (age >= 12 && age <= 18) {
    alert(`Вам ${age} лет и вы подросток`)
} else if (age >= 19 && age <= 40) {
    alert(`Вам ${age} лет и вы познаёте жизнь`)
} else if  (age >= 41 && age <= 80) {
    alert(`Вам ${age} лет и вы познали жизнь`)
} else if (age >= 81) {
    alert(`Вам ${age} лет и вы долгожитель`)
} else {
    alert('Неизвестное значение')
}
// extended
let expression = 'Я работаю со строками как профессионал!'
let count = 0

let words = expression.split(' ')

for (let i = 0; i < words.length; i++)
    if (words[i] != "") count++

console.log(count)