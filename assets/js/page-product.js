                                                            'use strict'

import { products } from "./data.js"

document.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('form')
    const button = form.querySelector('.btn')
    const [nameErrorText, ratingErrorText] = form.querySelectorAll('.error-text')
    const fields = form.querySelectorAll('.input')
    
    const objectForm = {
        'name': {
            validation: (field) => {
                if (field.validity.valueMissing) {
                    nameErrorText.textContent = 'Вы забыли указать имя и фамилию'
                } else if (field.validity.tooShort) {
                    nameErrorText.textContent = 'Имя не может быть короче 2-х символов'
                } else if (field.validity.patternMismatch) {
                    nameErrorText.textContent = 'Имя не может содержать пробелы до и после имени'
                }
                nameErrorText.className = 'error-text active'
            }
        },
        'rating': {
            validation: (field) => {
                if (field.validity.valueMissing) {
                    ratingErrorText.textContent = 'Оценка должна быть от 1 до 5'
                } else if (field.validity.badInput) {
                    ratingErrorText.textContent = 'Оценка должна быть от 1 до 5'
                } else if (field.validity.rangeUnderflow) {
                    ratingErrorText.textContent = 'Оценка должна быть от 1 до 5'
                } else if (field.validity.rangeOverflow) {
                    ratingErrorText.textContent = 'Оценка должна быть от 1 до 5'
                }
                ratingErrorText.className = 'error-text active'
            }
        },
        'comment': {}
    }
    
    function toggleButton() {
        if (form.checkValidity()) {
            button.disabled = false;
        } else {
            button.disabled = true;
        }
    }
    
    function onInputHandler(event) {
        const {target} = event
        const name = target.getAttribute('name')
        localStorage.setItem(name, target.value)
        if (target.tagName === "INPUT") {
            if(target.validity.valid) {
                const errorText = target.closest('label').querySelector('.error-text')
                errorText.textContent = ''
                errorText.className = 'error-text'
            } else {
                objectForm[name].validation(target)
            }
        }
        toggleButton()
    }
    
    function onBlurHandler(event) {
        const {target} = event
        if (target.tagName === "INPUT") {
            if (target.value === '') {
                const errorText = target.closest('label').querySelector('.error-text')
                errorText.textContent = ''
                errorText.className = 'error-text'
            }
        }
    }
    
    function resetData() {
        fields.forEach(field => field.value = localStorage.removeItem([field.name]))
    }
    
    function initForm() {
        fields.forEach(field => field.value = localStorage.getItem([field.name]))
    }
    
    function onSubmitHandler(event) {
        if(!form.checkValidity())
            event.preventDefault()
        resetData()
        initForm()
    }
    
    fields.forEach(field => field.addEventListener('blur', onBlurHandler))
    form.addEventListener('input', onInputHandler)
    form.addEventListener('submit', onSubmitHandler)
    
    initForm()
})