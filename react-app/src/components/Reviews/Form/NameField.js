import { forwardRef } from "react";

const NameField = forwardRef((props, ref) => {    
    const {error, errortext, ...restProps} = props;
    return (
        <label className="form__input-wrapper">
            <input ref={ref} {...restProps} className="form__input input" />
                {error && <span className="error-text">{errortext}</span>}
        </label>
    );
});

export default NameField;