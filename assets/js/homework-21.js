'use strict'

// check that an object is empty
function isEmpty(object) {
   
    if (Object.keys(object).length === 0) return true
    
    return false
    
}

// third

const salary = {
    John: 100000,
    Ann: 160000,
    Pete: 130000
}

function raiseSalary(percent) {

    for (let employee in salary) {
        let salaryValue = salary[employee]
        salary[employee] += Math.floor(salaryValue / 100 * percent)
    }

    return salary

}

raiseSalary(5)

/** show the summary of all employees salary */
let sum = 0

for (let employee in salary) {
    sum += salary[employee]
}

console.log(sum)