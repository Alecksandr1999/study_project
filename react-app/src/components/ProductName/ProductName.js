import '../Base.css';
import './ProductName.css';

const ProductName = ({text}) => <h2 className="product__name">{text}</h2>
export default ProductName;