import { forwardRef } from "react";

const RatingField = forwardRef((props, ref) => {
    const {error, errortext, ...restProps} = props;
    return (
        <label className="form__input-wrapper form__input-wrapper--rating">
            <input ref={ref} {...restProps} className="form__input input" />
                {error && <span className="error-text">{errortext}</span>}
        </label>
    );
});

export default RatingField;