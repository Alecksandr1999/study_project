const MemoryOption = ({ memory: {value, unit}, active, ...restProps }) => {
    const className = `property__button property__memory-button df ${active ? 'active' : ''}`;
    return (
        <li className="buttons__item df">
            <button type="button"
                    className={className}
                    {...restProps}>
                {value} {unit}
            </button>
        </li>
    );
};

export default MemoryOption;