import ReviewsHeader from "./ReviewsHeader/ReviewsHeader";
import Form from "./Form/Form";
import Review from "./Review/Review";

import "../Base.css";
import "./Reviews.css";

const Reviews = ({ props }) => {
    const reviews = props.map(review => {
        return <Review key={review.id} props={review} />
    });
    return (
        <section className="reviews">
            <ReviewsHeader />
            <div className="reviews__body">{reviews}</div>
            <Form />
        </section>
    );
};

export default Reviews;