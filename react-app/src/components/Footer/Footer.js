import '../Base.css';
import './Footer.css';

const Footer = () => {
    return (
        <footer className="footer">
            <div className="footer__inner container">
                <div className="footer__content">
                    <span className="copyright">
                        © ООО «<span className="copyright__logo"><span>Мой</span>Маркет</span>», 2018-2022.
                    </span>
                    <p className="footer__text">
                        Для уточнения информации звоните по номеру <a href="tel:79000000000">+7 900 000 0000</a>,<br/>
                        а предложения по сотрудничеству отправляйте на почту <a href="mailto:partner@mymarket.com">partner@mymarket.com</a>
                    </p>
                </div>
                <a href="#header" className="to-top-btn btn to-top-btn--footer">Наверх</a>
            </div>
        </footer>
    );
};

export default Footer;