import Header from '../Header/Header';
import Main from '../Main/Main';
import Footer from '../Footer/Footer';

import './PageProduct.css';

const PageProduct = () => {
    return (
        <div className="main-page" id="page">
            <Header />
            <Main />
            <Footer />
        </div>
    );
};

export default PageProduct;