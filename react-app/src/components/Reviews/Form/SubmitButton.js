export const SubmitButton = ({ isValid }) => 
    <button className="form__button btn" type="submit" disabled={isValid}>Отправить отзыв</button>;