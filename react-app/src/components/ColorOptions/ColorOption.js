const ColorOption = ({ color: { img: src, alt}, active, ...restProps }) => {
    const className = `property__button property__color-button df ${active ?
        'active' : ''}`;
    return (
        <li className="buttons__item df">
            <button
                type="button"
                className={className}
                {...restProps}>
                <img src={src} alt={alt} className="property__image" />
            </button>
        </li>
    );
};

export default ColorOption;