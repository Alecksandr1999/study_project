import data from '../../data.json';
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';
import ProductName from '../ProductName/ProductName';
import ProductGallery from '../ProductGallery/ProductGallery';
import ColorOptions from '../ColorOptions/ColorOptions';
import MemoryOptions from '../MemoryOptions/MemoryOptions';
import ProductOrder from '../ProductOrder/ProductOrder';
import ComparisingTable from '../ComparisonTable/ComparisonTable';
import Reviews from '../Reviews/Reviews';

import '../Base.css';
import './Main.css';

const Main = () => {
    return (
        <main className="main container main-page__main-block">
            <Breadcrumbs pages={data.pages} />
            {/* <!-- PRODUCT CARD --> */}
            <div className="product">
                <ProductName text={data.products[0].name} />
                <ProductGallery images={data.products[0].images}/>

                <div className="product__columns">
                    <div className="product__column product__column--left">
                        {/* <!-- Properties --> */}
                        <section className="product__properties properties">
                            {/* <!-- Color --> */}
                            <ColorOptions colors={data.products[0].colors} />
                            {/* <!-- Memory --> */}
                            <MemoryOptions memory={data.products[0].memory} />
                            {/* <!-- Features --> */}
                            <div className="property property--features">
                                <h3 className="property__name">Характеристики товара</h3>
                                <ul className="property__features-list"> 
                                    <li className="property__feature">Экран: <b>6.1</b></li>
                                    <li className="property__feature">Встроенная память: <b>128 ГБ</b></li>
                                    <li className="property__feature">Операционная система: <b>iOS 15</b></li>
                                    <li className="property__feature">Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b></li>
                                    <li className="property__feature">Процессор: <b><a href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank" rel="noreferrer">Apple A15 Bionic</a></b></li>
                                    <li className="property__feature">Вес: <b>173 г</b></li>
                                </ul>
                            </div>
                        </section>
                        {/* <!-- Description --> */}
                        <section className="description">
                            {/* <!-- description title --> */}
                            <h3 className="description__title">Описание</h3>
                            {/* <!-- description body --> */}
                            <div className="description__body">
                                <p className="description__body-paragraph">
                                    Наша самая совершенная система двух камер.<br />
                                    Особый взгляд на прочность дисплея.<br />
                                    Чип, с которым всё супербыстро.<br />
                                    Аккумулятор держится заметно дольше.<br />
                                    <i>iPhone 13 - сильный мира всего</i>.<br />
                                </p>
                                <p className="description__body-paragraph">Мы разработали совершенно новую схему расположения и развернули объективы на 45 градусов. Благодаря этому внутри корпуса поместилась нашалучшая система двух камер с увеличенной матрицей широкоугольной камеры. Кроме того, мы освободили место для системы оптической стабилизации изображения сдвигом матрицы. И повысили скорость работы матрицы на сверхширокоугольной камере.</p>
                                <p className="description__body-paragraph">Новая сверхширокоугольная камера видит больше деталей в тёмных областях снимков. Новая широкоугольная камера улавливает на 47% больше света для более качественных фотографий и видео. Новая оптическая стабилизация сосдвигом матрицы обеспечит чёткие кадры даже в неустойчивом положении.</p>
                                <p className="description__body-paragraph">Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещения фокуса и изменения резкости. Просто начните запись видео. Режим «Киноэффект» будет удерживать фокус на объекте съёмки, создавая красивый эффект размытиявокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другого человека или объект, который появился в кадре. Теперь ваши видео будут смотреться как настоящее кино.</p>
                            </div>
                        </section>
                        <ComparisingTable />
                        <Reviews props={data.reviews}/>
                    </div>{/* <!--/product__column_left--> */}
                    
                    <div className="product__column product__column--right">
                        <ProductOrder product={data.products[0]}/>
                        <aside className="ad">
                            <span className="ad__title">Реклама</span>
                            <iframe title="firstFrame" src="/ad.html" className="ad__item"></iframe>
                            <iframe title="secondFrame" src="/ad.html" className="ad__item"></iframe>
                        </aside>
                    </div>{/* <!--/product__column_right--> */}
                </div>{/* <!--/product__columns--> */}
            </div>{/* <!--/product--> */}
        </main>
    );
};

export default Main;