'use strict'

function getSum (array) {

    const filteredArray = array.filter(Number)

    return filteredArray.reduce((sum, current) => sum + current, 0)

}

let arr1 = [1, 2, 10, 5]
alert(getSum(arr1)) // 18

let arr2 = ["a", {}, 3, 3, -2]
alert(getSum(arr2)) // 4

// third task
let cart = [265, 46, 821]

function addToCart (productId) {
    
    if (!cart.includes(productId)) {
        cart.push(productId)
        return cart
    }
    
}

function removeFromCart (productId) {

    cart = cart.filter(id => id !== productId)
    return cart

}

addToCart(75)
removeFromCart(46)