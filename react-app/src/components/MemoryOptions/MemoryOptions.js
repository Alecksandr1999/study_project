import { useState } from 'react';
import MemoryOption from './MemoryOption';

import '../Base.css';
import './MemoryOptions.css';

const MemoryOptions = ({ memory }) => {
    const [activeMemoryOption, setActiveMemoryOption] = useState({"value": 128, "unit": "ГБ"});

    const handleClick = (value) => {
        setActiveMemoryOption(value);
    };

    return (
        <div className="property">
            <h3 className="property__name">Конфигурация памяти: <span className="property__value">{`${activeMemoryOption.value} ${activeMemoryOption.unit}`}</span></h3>
            <ul className="property__buttons">
                {memory.map(memoryOption => 
                    <MemoryOption key={memoryOption.value}
                                  memory={memoryOption}
                                  active={memoryOption.value === activeMemoryOption.value}
                                  onClick={() => handleClick({"value" : memoryOption.value, "unit":  memoryOption.unit})} />
                )}
            </ul>
        </div>
    );
};

export default MemoryOptions;