'use strict'

// first exercise
let number = +prompt('Введите число', '')
let errorText = 'Кажется, вы ввели неверные данные'

let promise = new Promise((result, reject) => {

    if(isNaN(number) || number === 0) {
        reject(errorText)
    }
    
    result(number)

})

promise
    .then(result => {
        const countdownTimer = setInterval(() => {

            console.log(`Осталось ${result}`)

            result--

            if (result === 0) {
                clearInterval(countdownTimer)
                console.log('Время вышло!')
            }

        }, 1000)

    })
    .catch(reject => {
        console.error(reject)
    })

// second exercise
console.time('timeTakesToGetResponse')
let getDataFromAPI = fetch('https://reqres.in/api/users')

getDataFromAPI
.then(response => {
    return response.json()
})
.then(response => {

    console.timeEnd('timeTakesToGetResponse')

    let data = response.data

    console.group('Пользователи')

    console.log(`Получили пользователей: ${data.length}`)
    for (let key in data) {
        console.log(data[key].first_name, data[key].last_name, `(${data[key].email})`)
    }
    console.groupEnd()
})
.catch(() => {
    console.error('Что-то пошло не так :(')
})
