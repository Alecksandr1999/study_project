import { forwardRef } from "react";

export const MessageField = forwardRef((props, ref) =>
    <textarea ref={ref} {...props} placeholder="Текст отзыва" className="form__textarea input"></textarea>);