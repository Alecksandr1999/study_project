'use strict'

// firstTask
const firstTask = () => {
    
    let a ='100px'
    let b ='323px'
    
    let result = parseInt(a) + parseInt(b)
    
    console.log(result)

}

firstTask()

// secondTask
const secondTask = () => {
    
    let result = Math.max(10, -45, 102, 36, 12, 0, -1)
    
    console.log(result)

}

secondTask()

// third task
const thirddTask = () => {
    
    let c = 123.3399
    console.log(Math.round(c))

    let d = 0.111
    console.log(Math.ceil(d))

    let e = 45.333333
    console.log(Number(e.toFixed(1)))

    let f = 3
    console.log(f**5)

    let g = 4e14
    console.log(Math.round(g))

    let h = '1' == 1
    console.log(h)

}

thirddTask()

// Не буду гуглить, кажется, дело в том как работает js, а работает он так, что 0.1 = 0.100000000000000000000000001 или что-то типо того
// Проблема дробных чисел, в общем.
console.log(0.1+0.2===0.3);// Вернёт false, почему?