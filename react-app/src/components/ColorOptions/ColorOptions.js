import { useState } from 'react';
import ColorOption from './ColorOption';

import '../Base.css';
import './ColorOptions.css';

const ColorOptions = ({ colors }) => {
    const [activeColor, setActiveColor] = useState('Чёрный');

    const handleClick = (name) => {
        setActiveColor(name);
    };

    return (
        <div className="property">
            <h3 className="property__name">Цвет товара: <span className="property__value">{activeColor}</span></h3>
            <ul className="property__buttons">
                {colors.map(color =>
                    <ColorOption key={color.name} color={color}
                            active={color.name === activeColor}
                            onClick={() => handleClick(color.name)} />)
                }
            </ul>
        </div>
    );
};

export default ColorOptions;
