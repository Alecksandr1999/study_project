import { ReactComponent as Heart } from './heart.svg';

import '../Base.css';
import './ProductOrder.css';

const ProductOrder = ({ product }) => {
    const options = product.deliveryOptions.map(option => {
        const { type, date, cost } = option;
        return <p key={type}>{type} в {date} — <strong>{cost ? cost : 'бесплатно'}</strong></p>;
    });
    return (
        <div className="product__order">
            <button type="button" className="add-to-favorite-btn">
                <Heart className="add-to-favorite-btn__icon" />
            </button>
            <div className="product__price price">
                <span className="price__value">{product.price}</span>
                <span className="price__discount">{product.discount}%</span>
                <span className="price__total">{product.totalPrice}₽</span>
            </div>
            <div className="product__delivery">{options}</div>
            <button id="product-button" type="button" className="add-to-bucket-btn btn">Добавить в корзину</button>
        </div>
    );
};

export default ProductOrder;