'use strict'

const evenNumbers = []

for (let number = 20; number >= 0; number--) {
    if (number % 2 === 0) evenNumbers.push(number)   
}

console.log(evenNumbers)

// second
let sum = 0

for (let index = 1; index <= 3; index++) {

    let value = +prompt('Введите число', '')
    
    if (isNaN(value) || value === '') {
        alert('Ошибка, вы ввели не число')
        break
    }

    sum += value

}

console.log(sum)

// third
const months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']

function getNameOfMonth(indexOfMonth) {
   
    return months[indexOfMonth]
}

getNameOfMonth(0)

// 3.5
const monthsButOctober = months.filter(month => month !== 'Октябрь')

console.log(monthsButOctober)
