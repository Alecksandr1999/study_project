import { Rating } from '@mui/material';

import '../../Base.css';
import './Review.css';

const Review = ({ props }) => {
    const {avatar, name, rating, userExperience, advantages, disadvantages} = props;
    return (
        <div className="review">
            <div className="review__avatar">
                <img src={avatar} alt="Фото рецензента" />
            </div>
            <div className="review__body">
                <h4 className="review__name">{name}</h4>
                <Rating className="review__rating" name="read-only" value={rating} readOnly size="large" />
                <div className="review__content">
                    <p><b>Опыт использования:</b> {userExperience}</p>
                    <p><b>Достоинства:</b><br />  {advantages}</p>
                    <p><b>Недостатки:</b><br /> {disadvantages}</p>
                </div>
            </div>
        </div>
    );
};

export default Review;