import '../Base.css';
import './ProductGallery.css';

const Gallery = ({ images: { black } }) => {
    const listItems = black.map(image => {
        const { src, alt } = image;
        return (
            <li key={src} className="product__gallery-item">
                <img src={src} alt={alt} className="product__gallery-img" />
            </li>
        );
    });
    return (<ul className="product__gallery">{listItems}</ul>);
};

export default Gallery;