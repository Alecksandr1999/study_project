import '../../Base.css';
import './ReviewsHeader.css';

const ReviewsHeader = () => {
    return (
        <div className="reviews__header">
            <h3 className="reviews__counter">
                Отзывы <span className="reviews__counter-value">425</span>
            </h3 >
        </div>
    );
};

export default ReviewsHeader;