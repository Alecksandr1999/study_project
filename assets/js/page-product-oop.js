'use strict'

class Form {
    constructor(node) {
        this.dom = node
        this.fields = [...node.querySelectorAll('.input')].reduce((accum, field, index) => ({
            ...accum,
            [index]: new Field(field, index, this)
        }), {})
        this.submitButton = new SubmitButton(node.querySelector('.btn'), this.dom)
        this.onSubmit = this.onSubmit.bind(this)
        this.dom.addEventListener('submit', this.onSubmit)
        return this
    }
    resetData() {
        for (let value of Object.values(this.fields)) {
            value.dom.value = localStorage.removeItem([value.dom.name])
        }
    }
    initForm() {
        for (let value of Object.values(this.fields)) {
            value.dom.value = localStorage.getItem([value.dom.name])
        }
    }
    onSubmit() {
        this.resetData()
        this.initForm()
    }
}

class Field {
    constructor(node, id, form) {
        this.dom = node
        this.id = id
        this.form = form
        this.onInput = this.onInput.bind(this)
        this.dom.addEventListener('input', this.onInput)
        this.onBlur = this.onBlur.bind(this)
        this.dom.addEventListener('blur', this.onBlur)
        return this
    }
    validation(type) {
        const error = this.dom.nextElementSibling
        if (type === 'name') {
            if (this.dom.validity.valueMissing) {
                error.textContent = 'Вы забыли указать имя и фамилию'
            } else if (this.dom.validity.tooShort) {
                error.textContent = 'Имя не может быть короче 2-х символов'
            } else if (this.dom.validity.patternMismatch) {
                error.textContent = 'Имя не может содержать пробелы до и после имени'
            }
            error.className = 'error-text active'
        }
        if (type === 'rating') {
            if (this.dom.validity.valueMissing) {
                error.textContent = 'Оценка должна быть от 1 до 5'
            } else if (this.dom.validity.badInput) {
                error.textContent = 'Оценка должна быть от 1 до 5'
            } else if (this.dom.validity.rangeUnderflow) {
                error.textContent = 'Оценка должна быть от 1 до 5'
            } else if (this.dom.validity.rangeOverflow) {
                error.textContent = 'Оценка должна быть от 1 до 5'
            }
            error.className = 'error-text active'
        }
    }
    onInput(event) {
        const {target} = event
        const name = target.getAttribute('name')
        localStorage.setItem(name, target.value)
        if (target.tagName === "INPUT") {
            if(target.validity.valid) {
                const errorText = target.closest('label').querySelector('.error-text')
                errorText.textContent = ''
                errorText.className = 'error-text'
            } else {
                this.validation(name)
            }
        }
        this.form.submitButton.toggleButton()
    }
    onBlur(event) {
        const {target} = event
        if (target.tagName === "INPUT") {
            if (target.value === '') {
                const errorText = target.closest('label').querySelector('.error-text')
                errorText.textContent = ''
                errorText.className = 'error-text'
            }
        }
    }
}

class SubmitButton {
    constructor(node, form) {
        this.dom = node
        this.form = form
        return this
    }
    toggleButton() {
        if (this.form.checkValidity()) {
            this.dom.disabled = false;
        } else {
            this.dom.disabled = true;
        }
    }
}

// const reviewForm = new Form(document.getElementById('form'))

// What if we need more unique?
class Input {
    constructor(node, validationParams = null) {
        this.dom = node
        this.error = node.nextElementSibling
        this.validationParams = validationParams
        this.onInput = this.onInput.bind(this)
        this.dom.addEventListener('input', this.onInput)
        this.onBlur = this.onBlur.bind(this)
        this.dom.addEventListener('blur', this.onBlur)
        return this
    }
    validation(validationParams) {
        for (let key in validationParams) {
            if (this.dom.validity[key]) this.error.textContent = validationParams[key]
        }
        this.error.className = 'error-text active'
    }
    onInput() {
        const name = this.dom.getAttribute('name')
        localStorage.setItem(name, this.dom.value)
        if(this.dom.validity.valid) {
            this.error.textContent = ''
            this.error.className = 'error-text'
        } else {
            this.validation(this.validationParams)
        }
    }
    onBlur() {
        if (this.dom.value === '') {
            this.error.textContent = ''
            this.error.className = 'error-text'
        }
    }
}

// const nameInput = new Input(document.querySelector('.input[name="name"]'), {
//     valueMissing: 'Вы забыли указать имя и фамилию',
//     tooShort: "Имя не может быть короче 2-х символов",
//     patternMismatch: "Имя не может содержать пробелы до и после имени"
// })
// const ratingInput = new Input(document.querySelector('.input[name="rating"]'), {
//     valueMissing: "Оценка должна быть от 1 до 5",
//     badInput: "Оценка должна быть от 1 до 5",
//     rangeUnderflow: "Оценка должна быть от 1 до 5",
//     rangeOverflow: "Оценка должна быть от 1 до 5"
// })
// const messageInput = new Input(document.querySelector('.input[name="message"]'))

// ... or way more?
class BaseInput {
    constructor(node) {
        this.dom = node
        this.onInput = this.onInput.bind(this)
        this.dom.addEventListener('input', this.onInput)
        return this
    }
    onInput() {
        localStorage.setItem(this.dom.getAttribute('name'), this.dom.value)
    }
}

class VerifiableInput extends BaseInput {
    constructor(node, validationParams = null) {
        super(node)
        this.error = node.nextElementSibling
        this.validationParams = validationParams
        this.onBlur = this.onBlur.bind(this)
        this.dom.addEventListener('blur', this.onBlur)
        return this
    }
    validation(validationParams) {
        for (let key in validationParams) {
            if (this.dom.validity[key]) this.error.textContent = validationParams[key]
        }
        this.error.className = 'error-text active'
    }
    onInput() {
        localStorage.setItem(this.dom.getAttribute('name'), this.dom.value)
        if(this.dom.validity.valid) {
            this.error.textContent = ''
            this.error.className = 'error-text'
        } else {
            this.validation(this.validationParams)
        }
    }
    onBlur() {
        if (this.dom.value === '') {
            this.error.textContent = ''
            this.error.className = 'error-text'
        }
    }
}

// const nameInput = new VerifiableInput(document.querySelector('.input[name="name"]'), {
//     valueMissing: 'Вы забыли указать имя и фамилию',
//     tooShort: "Имя не может быть короче 2-х символов",
//     patternMismatch: "Имя не может содержать пробелы до и после имени"
// })
// const ratingInput = new VerifiableInput(document.querySelector('.input[name="rating"]'), {
//     valueMissing: "Оценка должна быть от 1 до 5",
//     badInput: "Оценка должна быть от 1 до 5",
//     rangeUnderflow: "Оценка должна быть от 1 до 5",
//     rangeOverflow: "Оценка должна быть от 1 до 5"
// })
// const messageInput = new BaseInput(document.querySelector('.input[name="message"]'))

// THAT ENOUGH SLICES!!!
class AbstractForm {
    constructor(node, validationParams = null) {
        this.dom = node
        this.validationParams = validationParams // unnecessary?
        this.verifiableInputs = [...document.querySelectorAll('.input')].reduce((accum, input) => {
            input.classList.contains('verifiableInput') ?
                accum[input.name] = new VerifiableBasicInput(input, this, validationParams[input.name]) :
                accum[input.name] = new BasicInput(input)
            return accum
        }, {})
        this.AbstractSubmitButton = new AbstractSubmitButton(node.querySelector('.btn'), this.dom)
        this.onSubmit = this.onSubmit.bind(this)
        this.dom.addEventListener('submit', this.onSubmit)
        return this
    }
    resetData() {
        for (let input of Object.values(this.verifiableInputs)) {
            input.dom.value = localStorage.removeItem([input.dom.name])
        }
    }
    initForm() {
        for (let input of Object.values(this.verifiableInputs)) {
            input.dom.value = localStorage.getItem([input.dom.name])
        }
    }
    onSubmit() {
        this.resetData()
        this.initForm()
    }
}

class BasicInput {
    constructor(node) {
        this.dom = node
        this.onInput = this.onInput.bind(this)
        this.dom.addEventListener('input', this.onInput)
        return this
    }
    onInput() {
        localStorage.setItem(this.dom.name, this.dom.value)
    }
}

class VerifiableBasicInput extends BasicInput{
    constructor(node, form, validationParams) {
        super(node)
        this.dom = node
        this.form = form
        this.error = node.nextElementSibling
        this.validationParams = validationParams // unnecessary?
        this.onInput = this.onInput.bind(this)
        this.dom.addEventListener('input', this.onInput)
        this.onBlur = this.onBlur.bind(this)
        this.dom.addEventListener('blur', this.onBlur)
        return this
    }
    validation(validationParams) {
        if(!validationParams) return false
        for (let key in validationParams) {
            if (this.dom.validity[key]) this.error.textContent = validationParams[key]
        }
        this.error.className = 'error-text active'
    }
    onInput() {
        localStorage.setItem(this.dom.name, this.dom.value)
        if(this.dom.validity.valid) {
            this.error.textContent = ''
            this.error.className = 'error-text'
        } else {
            this.validation(this.validationParams)
        }
        this.form.AbstractSubmitButton.toggleButton()
    }
    onBlur() {
        if (this.dom.value === '') {
            this.error.textContent = ''
            this.error.className = 'error-text'
        }
    }
}

class AbstractSubmitButton {
    constructor(node, form) {
        this.dom = node
        this.form = form
        return this
    }
    toggleButton() {
        if (this.form.checkValidity()) {
            this.dom.disabled = false;
        } else {
            this.dom.disabled = true;
        }
    }
}

const parameters = {
    name: {
        valueMissing: 'Вы забыли указать имя и фамилию',
        tooShort: "Имя не может быть короче 2-х символов",
        patternMismatch: "Имя не может содержать пробелы до и после имени"
    },
    rating: {
        valueMissing: "Оценка должна быть от 1 до 5",
        badInput: "Оценка должна быть от 1 до 5",
        rangeUnderflow: "Оценка должна быть от 1 до 5",
        rangeOverflow: "Оценка должна быть от 1 до 5"
    }
}

const myAwesomeForm = new AbstractForm(document.getElementById('form'), parameters)