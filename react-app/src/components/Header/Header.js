import logo from '../../images/logo.svg';
import { ReactComponent as Cart } from './cart.svg';

import '../Base.css';
import './Header.css';

const Header = () => {
    return(
        <header id="header" className="header main-page__header-block">
            <div className="header__inner container">
                <h1 className="header__logo">
                    <img src={logo} alt="МойМаркет" className="logo logo--header" />
                </h1>
                <a href="user-cart.html?userId=1" data-products="0" className="cart header__cart">
                    <Cart className="cart__icon" />
                </a>
            </div>
        </header>
    );
};

export default Header;