import '../Base.css';
import './Breadcrumbs.css';

const Breadcrumbs = ({ pages }) => {
    const links = pages.map(link => {
        const { path: href, title } = link;
        return (<a key={title} href={href} className="breadcrumb">{title}</a>);
    });
    return (<nav className="breadcrumbs">{links}</nav>);
};

export default Breadcrumbs;