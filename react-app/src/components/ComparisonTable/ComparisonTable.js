import '../Base.css';
import './ComparisonTable.css';

const ComparisingTable = () => {
    return (
        <section className="comparing">
            <table className="comparing__table table">
                <caption className="table__caption">
                    <h3 className="table__cation-title">Сравнение моделей</h3>
                </caption>
                <tbody>
                    <tr className="table__row table__row--head">
                        <th className="table__cell table__cell--head">Модель</th>
                        <th className="table__cell table__cell--head">Вес</th>
                        <th className="table__cell table__cell--head">Высота</th>
                        <th className="table__cell table__cell--head">Ширина</th>
                        <th className="table__cell table__cell--head">Толщина</th>
                        <th className="table__cell table__cell--head">Чип</th>
                        <th className="table__cell table__cell--head">Объём памяти</th>
                        <th className="table__cell table__cell--head">Аккумулятор</th>
                    </tr>
                    <tr className="table__row">
                        <td className="table__cell">iPhone 11</td>
                        <td className="table__cell">194 грамма</td>
                        <td className="table__cell">150.9 мм</td>
                        <td className="table__cell">75.7 мм</td>
                        <td className="table__cell">8.3 мм</td>
                        <td className="table__cell">A13 Bionic chip</td>
                        <td className="table__cell">до 128 Гб</td>
                        <td className="table__cell">До 17 часов</td>
                    </tr>
                    <tr className="table__row">
                        <td className="table__cell">iPhone 12</td>
                        <td className="table__cell">164 грамма</td>
                        <td className="table__cell">146.7 мм</td>
                        <td className="table__cell">71.5 мм</td>
                        <td className="table__cell">7.4 мм</td>
                        <td className="table__cell">A14 Bionic chip</td>
                        <td className="table__cell">до 256 Гб</td>
                        <td className="table__cell">До 19 часов</td>
                    </tr>
                    <tr className="table__row">
                        <td className="table__cell">iPhone 13</td>
                        <td className="table__cell">174 грамма</td>
                        <td className="table__cell">146.7 мм</td>
                        <td className="table__cell">71.5 мм</td>
                        <td className="table__cell">7.65 мм</td>
                        <td className="table__cell">A15 Bionic chip</td>
                        <td className="table__cell">до 512 Гб</td>
                        <td className="table__cell">До 19 часов</td>
                    </tr>
                </tbody>
            </table>
        </section>
    );
};

export default ComparisingTable;
