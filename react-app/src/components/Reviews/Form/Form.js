import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import NameField from './NameField';
import RatingField from './RatingField';
import { MessageField } from './MessageField';
import { SubmitButton } from './SubmitButton';
import '../../Base.css';
import './Form.css';

const ReviewSchema = yup.object().shape({
    name: yup.string()
      .matches(/^(?!.*[ \t]$)\S.*$/, "Имя не может содержать пробелы до и после имени")
      .min(2, "Имя не может быть короче 2-х символов")
      .required("Вы забыли указать имя и фамилию"),
    rating: yup.number()
      .typeError("Оценка должна быть от 1 до 5")
      .min(1, "Оценка должна быть от 1 до 5")
      .max(5, "Оценка должна быть от 1 до 5")
      .required("Оценка должна быть от 1 до 5"),
});

const Form = () => {
    const { register, handleSubmit, formState: { isValid, errors }, reset, watch, setValue } = useForm({
        mode: 'onBlur', resolver: yupResolver(ReviewSchema), defaultValues: {
            "name": '', "rating": '', "message": ''
        }
    });
    
    const onSubmit = () => reset();

    useEffect(() => {
        const subscription = watch((value) => {
            localStorage.setItem('reviewForm', JSON.stringify(value));
        });
        return () => subscription.unsubscribe();
    }, [watch]);
   
    useEffect(() => {
        for (let entry of Object.entries(JSON.parse(localStorage.getItem('reviewForm')))) {
            setValue(entry[0], entry[1]);
        }
    }, [setValue]);

    return (
        <form className="reviews__form form" onSubmit={handleSubmit(onSubmit)}>
            <fieldset className="form__group">
                <legend className="form__caption">Добавить свой отзыв</legend>
                <div className="form__inputs">
                    <NameField
                        type="text"
                        placeholder="Имя и фамилия"
                        {...register("name")}
                        error={!!errors.name}
                        errortext={errors?.name?.message}
                    /> 
                    <RatingField
                        type="number"
                        step="1"
                        placeholder="Оценка"
                        {...register("rating")}
                        error={!!errors.rating}
                        errortext={errors?.rating?.message}
                    />
                    <MessageField {...register("message")} />
                </div>
            </fieldset>
            <SubmitButton isValid={!isValid} />
        </form>
    );
};

export default Form;